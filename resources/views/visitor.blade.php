<!DOCTYPE html>
<html>
    <head>
        <!-- <meta https-equiv="refresh" content="30" > -->
        <link type="image/png" href="{{ URL::asset('../resources/assets/img/fav.png') }}">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
  
        <title>GMS New Visitor</title>

        <link href="httpss://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <!-- Bootstrap core CSS     -->
        <link href="{{ URL::asset('../resources/assets/css/bootstrap.min.css') }}" rel="stylesheet" />

        <!-- Animation library for notifications   -->
        <link href="{{ URL::asset('../resources/assets/css/animate.min.css') }}" rel="stylesheet"/>

        <!--  Light Bootstrap Table core CSS    -->
        <link href="{{ URL::asset('../resources/assets/css/light-bootstrap-dashboard.css') }}" rel="stylesheet"/>


        <!--  CSS for Demo Purpose, don't include it in your project     -->
        <link href="{{ URL::asset('../resources/assets/css/demo.css') }}" rel="stylesheet" />


        <!--     Fonts and icons     -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
        <link href="{{ URL::asset('../resources/assets/css/pe-icon-7-stroke.css') }}" rel="stylesheet" />

        <!-- International prefix -->
        <link rel="stylesheet" href="{{ URL::asset('../resources/assets/css/intlTelInput.css') }}">   

        <!-- Date picker -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

    </head>
    
    <body>
    <div class="wrapper">
        <div class="sidebar" data-color="purple" data-image="{{ URL::asset('../resources/assets/img/sidebar-5.jpg') }}">

            <!-- Side menu -->
            <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="" class="simple-text">
                        GMS
                    </a>
                </div>

                <ul class="nav">
                    <li>
                        <a href="{{ url('/') }}">
                        <i class="pe-7s-note2"></i>
                            <p>Guests In</p>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/guests') }}">
                            <i class="pe-7s-news-paper"></i>
                            <p>Guests</p>
                        </a>
                    </li>
                    <li class="active">
                        <a href="{{ url('/visitor') }}">
                            <i class="pe-7s-user"></i>
                            <p>Add Visitor</p>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/staff') }}">
                            <i class="pe-7s-science"></i>
                            <p>Staff</p>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/card') }}">
                            <i class="pe-7s-graph"></i>
                            <p>Cards</p>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/reports') }}">
                            <i class="pe-7s-map-marker"></i>
                            <p>Reports</p>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- !. Side menu -->

        </div>


        <!-- Table -->
        <div class="main-panel">
            <nav class="navbar navbar-default navbar-fixed">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">Guests</a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-dashboard"></i>
                                    <p class="hidden-lg hidden-md">Dashboard</p>
                                </a>
                            </li>
                        </ul>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <p>
                                            Options
                                            <b class="caret"></b>
                                        </p>

                                  </a>
                                  <ul class="dropdown-menu">
                                        <li><a href="{{ url('/register') }}">Register User</a></li>
                                      </ul>
                            </li>
                            <li>
                                <a href="{{ url('/logout') }}">
                                    <p>Log out</p>
                                </a>
                            </li>
                            <li class="separator hidden-lg hidden-md"></li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="header">
                                    <h4 class="title">New Visitor</h4>
                                </div>
                                <div class="content">

                                    <center>
                                        @if($errors->any())
                                        <ul style="color: red;">
                                                {{implode('', $errors->all('<li>:message</li>'))}}
                                        </ul>

                                        @endif

                                        @if (Session::has('message'))
                                        
                                        <p>{{ Session::get('message') }}</p>

                                        @endif

                                        {{Form::open(array('url' => 'register_visitor','enctype' => 'multipart/form-data')) }}

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>ID / PP Number:</label>

                                                    <p>{{ Form::text('national_id', '', array('class'=>'form-control', 'required'=> '', 'min'=>'1')) }}</p>
                                                </div>
                                            </div>
                                               
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>First Name:</label>

                                                    <p>{{ Form::text('f_name', '', array('class'=>'form-control', 'required'))  }}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Last Name:</label>
                                                    
                                                    <p>{{ Form::text('l_name', '', array('class'=>'form-control', 'required')) }}</p>
                                                </div>
                                            </div>
    
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Cell Number:</label>

                                                    <p>{{ Form::number('guest_cell', '', array('class'=>'form-control', 'required', 'min'=>'1')) }}</p> 
                                                </div>
                                            </div>                                          

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Person of Interest</label>                                        

                                                    <p>
                                                        <select class="form-control" required id="employee_id" name="employee_id" >
                                                            <option selected disabled>Kindly select one</option>
                                                            @foreach($staff as $s)
                                                                <option value="{{$s->id}}">{{$s->f_name}} {{$s->l_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Phone Number</label>  
                                            
                                                    <p>{{ Form::input('tel','phone_no', '',['id'=>'phone_no', 'class'=>'form-control']) }}</p>

                                                </div>
                                            </div> 

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Email Address</label>  
                                            
                                                    <p>{{ Form::input('email', 'visitee_email', '', ['id'=>'visitee_email', 'class'=>'form-control']) }}</p>

                                                </div>
                                            </div> 
                                            
                                            <!-- Time in hidden -->
                                            <p><input type="text" name="time_in" value="{{date('Y-m-d H:i:s')}}" hidden></p>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Passport photo (front)</label>

                                                    <p>{{ Form::file('passport_photo', '', array('class'=>'form-control', 'required')) }}</p>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Card Issued</label>

                                                    <p>
                                                        <select class="form-control" required id="card_id" name="card_id" >
                                                            <option selected disabled>Kindly select one</option>
                                                            @foreach($card as $c)
                                                                <option value="{{$c->id}}">{{$c->card_number}}</option>
                                                            @endforeach
                                                        </select>
                                                    </p>
                                                </div>
                                            </div>

                                            <p>{{ Form::submit('Submit',['class'=>'btn btn-info btn-fill']) }}</p>

                                        {{ Form::close() }}
 
                                    </center>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <nav class="pull-left">
                        <ul>
                            <li>
                                <a href="{{ url('/public') }}/">
                                    Home
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <p class="copyright pull-right">
                        &copy; <script>document.write(new Date().getFullYear())</script> <a href=""></a>
                    </p>
                </div>
            </footer>
        </div>
    </body>
    
    <!--   Core JS Files   -->
    <script src="{{ URL::asset('../resources/assets/js/jquery-1.10.2.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('../resources/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>


    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="{{ URL::asset('../resources/assets/js/bootstrap-checkbox-radio-switch.js') }}"></script>

    <!--  Charts Plugin -->
    <script src="{{ URL::asset('../resources/assets/js/chartist.min.js') }}"></script>

    <!--  Notifications Plugin    -->
    <script src="{{ URL::asset('../resources/assets/js/bootstrap-notify.js') }}"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="{{ URL::asset('../resources/assets/js/light-bootstrap-dashboard.js') }}"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="{{ URL::asset('../resources/assets/js/demo.js') }}"></script>


    <!-- International telephone script -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="{{ URL::asset('../resources/assets/js/intlTelInput.js') }}"></script>
    <script>
      $("#phone_no").intlTelInput({
        utilsScript: "{{ URL::asset('../resources/assets/js/utils.js') }}"
      });
    </script>
    <!-- End of international telephone -->

    
    <!-- Ajax on ready state change on selection -->
    <script type="text/javascript">

        $(document).ready(function(){

            $('#employee_id').change(function(){
                var str = $(this).val();

                // get staff phone numbers
                $.ajax({
                    type : 'get',
                    url : 'getnumber?number='+str,
                    success: function(data){
                       $('#phone_no').val(data);
                    }
                });

                // Get email address
                $.ajax({
                    type : 'get',
                    url : 'getemail?email='+str,
                    success: function (data) {
                        $('#visitee_email').val(data);
                    }
                });
            })

            /*National ID*/

            $("input[name*='national_id']").blur(function(){

                // console.log('here we have a blur');
                var str2 = $(this).val();

                $.ajax({
                    type : 'get',
                    url : 'getnationalid?nationalid='+str2,
                    success: function (data) {
                        // console.log(str2);
                        // alert("Returning Guest!");
                        if (data == 1) {
                            alert("Returning Guest!");
                        } else {
                            alert("New Guest!");
                        }

                    }
                })

            });

        })

    </script>
    <!-- !.Ajax on ready state change on selection -->

    <!-- Date Picker -->
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<!--     <script>
        $(function() {
            $( "#datepicker" ).datepicker({
                changeMonth: true,
                changeYear: true,
            });
    });
    </script> -->
    <!-- End of date picker -->
    
    <!-- Not working currently -->
    <!-- Javascript show/ hide method of communication -->
    <script type="text/javascript">
        function ShowHideDiv(chkEmail) {
            var dvPassport = document.getElementById("dvPassport");
            dvPassport.style.display = chkPassport.checked ? "block" : "none";
        }

        function ShowHideDiv(chkPhone) {
            var dvPassport = document.getElementById("dvPassport");
            dvPassport.style.display = chkPassport.checked ? "block" : "none";
        }
    </script>
    <!-- !.Javascript show/ hide method of communication -->

</html>