<!DOCTYPE html>
<html>
    <head>
    	<title>New Vistor</title>

    	<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
                font-weight: bold;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
	</head>

	<body>
		<div class="container">
		    <div class="content">

		    	@if($errors->any())
				<ul style="color: red;">
						{{implode('', $errors->all('<li>:message</li>'))}}
				</ul>

				@endif

				@if (Session::has('message'))
				
				<p>{{ Session::get('message') }}</p>

				@endif

				{{Form::open(array('url' => 'doLogin')) }}

					<p>Username:</p>

					<p>{{ Form::text('username') }}</p>

					<p>Password:</p>

					<p>{{ Form::password('password') }}</p>

					<p>{{ Form::submit('Submit') }}</p>

				{{ Form::close() }}

                
                <a href="http://localhost/hello-world/public/register">Register User</a>  &nbsp &nbsp
		    </div>
		</div>
	</body>	
</html>