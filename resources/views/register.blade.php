<!DOCTYPE html>
<?php
$page = $_SERVER['PHP_SELF'];
$sec = "25";
?>
<html>
    <head>
        <meta http-equiv="refresh" content="30" >
        <link type="image/png" href="{{ URL::asset('../resources/assets/img/fav.png') }}">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
  
        <title>GMS Visitors Registra</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

   

        <!-- Bootstrap core CSS     -->
        <link href="{{ URL::asset('../resources/assets/css/bootstrap.min.css') }}" rel="stylesheet" />

        <!-- Animation library for notifications   -->
        <link href="{{ URL::asset('../resources/assets/css/animate.min.css') }}" rel="stylesheet"/>

        <!--  Light Bootstrap Table core CSS    -->
        <link href="{{ URL::asset('../resources/assets/css/light-bootstrap-dashboard.css') }}" rel="stylesheet"/>


        <!--  CSS for Demo Purpose, don't include it in your project     -->
        <link href="{{ URL::asset('../resources/assets/css/demo.css') }}" rel="stylesheet" />


        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
        <link href="{{ URL::asset('../resources/assets/css/pe-icon-7-stroke.css') }}" rel="stylesheet" />

    </head>
    
	<body>
		<div class="container">
		    <div class="content">

		    	@if($errors->any())
				<ul style="color: red;">
						{{implode('', $errors->all('<li>:message</li>'))}}
				</ul>

				@endif

				@if (Session::has('message'))
				
				<p>{{ Session::get('message') }}</p>

				@endif

				{{Form::open(array('url' => 'register_action')) }}
                    
					<p>First Name:</p>

					<p>{{ Form::text('f_name') }}</p>

                    <p>Last Name:</p>

                    <p>{{ Form::text('l_name') }}</p>

					<p>Email:</p>

					<p>{{ Form::email('email') }}</p>

                    <p>ID / PP Number:</p>

                    <p>{{ Form::text('national_id') }}</p>

                    <p>Username</p>

                    <p>{{ Form::password('username') }}</p>

					<p>Password:</p>

					<p>{{ Form::password('password') }}</p>

					<p>Confirm Password:</p>

					<p>{{ Form::password('cpassword') }}</p>

					<p>{{ Form::submit('Submit') }}</p>

				{{ Form::close() }}
		    </div>
		</div>
    </body>
    
    <!--   Core JS Files   -->
    <script src="{{ URL::asset('../resources/assets/js/jquery-1.10.2.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('../resources/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="{{ URL::asset('../resources/assets/js/bootstrap-checkbox-radio-switch.js') }}"></script>

    <!--  Charts Plugin -->
    <script src="{{ URL::asset('../resources/assets/js/chartist.min.js') }}"></script>

    <!--  Notifications Plugin    -->
    <script src="{{ URL::asset('../resources/assets/js/bootstrap-notify.js') }}"></script>

</html>