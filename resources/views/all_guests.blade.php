<!DOCTYPE html>
<html>
    <head>
        <!-- <meta http-equiv="refresh" content="30" > -->
        <link type="image/png" href="{{ URL::asset('../resources/assets/img/fav.png') }}">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
  
        <title>GMS Visitors Registra</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <!-- Datatables -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">

        <!-- Bootstrap core CSS     -->
        <link href="{{ URL::asset('../resources/assets/css/bootstrap.min.css') }}" rel="stylesheet" />

        <!-- Animation library for notifications   -->
        <link href="{{ URL::asset('../resources/assets/css/animate.min.css') }}" rel="stylesheet"/>

        <!--  Light Bootstrap Table core CSS    -->
        <link href="{{ URL::asset('../resources/assets/css/light-bootstrap-dashboard.css') }}" rel="stylesheet"/>


        <!--  CSS for Demo Purpose, don't include it in your project     -->
        <link href="{{ URL::asset('../resources/assets/css/demo.css') }}" rel="stylesheet" />


        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
        <link href="{{ URL::asset('../resources/assets/css/pe-icon-7-stroke.css') }}" rel="stylesheet" />

        <!-- Search -->
        <style> 
        input[type=text] {
            width: 130px;
            box-sizing: border-box;
            border: 1px solid #ccc;
            border-radius: 4px;
            font-size: 12px;
            background-color: white;
            background-position: 100px 10px; 
            background-repeat: no-repeat;
            padding: 5px 2px 5px 20px;
            -webkit-transition: width 0.4s ease-in-out;
            transition: width 0.4s ease-in-out;
        }

        input[type=text]:focus {
            width: 30%;
        }
        </style>
        <!-- End of search -->

    </head>
   
    <body>
        <div class="wrapper">
            <div class="sidebar" data-color="purple" data-image="{{ URL::asset('../resources/assets/img/sidebar-5.jpg') }}">

                <!-- Side menu -->
                <div class="sidebar-wrapper">
                    
                    <ul class="nav">
                        <li>
                            <a href="{{ url('/') }}">
                            <i class="pe-7s-note2"></i>
                                <p>Guests In</p>
                            </a>
                        </li>
                        <li class="active">
                            <a href="{{ url('/guests') }}">
                                <i class="pe-7s-news-paper"></i>
                                <p>Guests</p>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/visitor') }}">
                                <i class="pe-7s-user"></i>
                                <p>Add Visitor</p>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/staff') }}">
                                <i class="pe-7s-science"></i>
                                <p>Staff</p>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/card') }}">
                                <i class="pe-7s-graph"></i>
                                <p>Cards</p>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/reports') }}">
                                <i class="pe-7s-map-marker"></i>
                                <p>Reports</p>
                            </a>
                        </li>  
                    </ul>
                </div>
                <!-- !. Side menu -->

            </div>


            <!-- Table -->
            <div class="main-panel">
                <nav class="navbar navbar-default navbar-fixed">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">Guests</a>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-left">
                                <li>
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-dashboard"></i>
                                        <p class="hidden-lg hidden-md">Dashboard</p>
                                    </a>
                                </li>
                            </ul>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <p>
                                            Options
                                            <b class="caret"></b>
                                        </p>

                                      </a>
                                      <ul class="dropdown-menu">
                                        <li><a href="{{ url('/register') }}">Register User</a></li>
                                        <li><a href="#">My Profile</a></li>
                                      </ul>
                                </li>
                                <li>
                                    <a href="{{ url('/logout') }}">
                                        <p>Log out</p>
                                    </a>
                                </li>
                                <li class="separator hidden-lg hidden-md"></li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="header">
                                        <h4 class="title">All Guests</h4>
                                        <p class="category">Guest Management System</p>
                                    </div>
                                    <div class="content table-responsive table-full-width">

                                        <table class="table table-hover table-striped" id="all_guests">
                                            <thead>
                                                <th>ID/ PP No.</th>
                                                <th>Visitor Name</th>
                                                <th>Staff Visited</th>
                                                <th>Time In</th>
                                                <th>Time Out</th>
                                                <th>Return Visit To</th>
                                                <!--<th hidden></th>-->
                                                <th>Tag Issued</th>
                                                <th>Options</th>
                                            </thead>
                                            <tbody>
                                            <?php $jk = 0; ?>
                                                  @foreach($guests as $g)
                                                    
                                                    <tr>
                                                        <td>{{$g->national_id}}</td>
                                                        <td><a href="mgeni/{{$g->id}}">{{$g->f_name}} {{$g->l_name}}</a></td>
                                                        <td>{{$g->empfname}} {{$g->emplname}}</td>
                                                        <td>{{$g->time_in}}</td>
                                                        <td>{{$g->time_out}}</td>
                                                        <form id="form-{{$g->id}}" name="form-{{$g->id}}" method="post" action="return_visitor" >
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <input type="hidden" name="id" value="{{$g->id}}" />
                                                            <td>
                                
                                                                <select required="required" id="employee_id{{$jk}}" name="employee_id" class="employee_id">
                                                                    <option value="" selected disabled>Kindly select one</option>
                                                                    @foreach($staff as $s)
                                                                        <option value="{{ $s->id }}">{{ $s->f_name }} {{ $s->l_name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                            
                                                            <!-- Hidden cells -->
                                                            <!--<td hidden> -->
                                                                <input type="hidden" class="phone_no" name="phone_no" />
                                                                <input type="hidden" class="e_f_name" name="e_f_name" />
                                                                <input type="hidden" class="e_l_name" name="e_l_name"/>
                                                                <input type="hidden" class="e_email" name="e_email" />
                                                                <input type="hidden" name="g_f_name" value="{{$g->f_name}}" />
                                                                <input type="hidden" name="g_l_name" value="{{$g->l_name}}" />
                                                                <input type="hidden" class="g_cell" name="g_cell" value="{{$g->guest_cell}}" />
                                                            <!--</td>-->
                                                            
                                                            <td>
                                                                <select required="required" id="card_id" name="card_id" class="btn btn-info btn-fill">
                                                                    <option value="" selected disabled>Select One</option>
                                                                    @foreach($visitor_card as $v_c)
                                                                        <option value="{{ $v_c->id }}">{{ $v_c->id }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </td>

                                                            <td>
                                                            <button type="submit" class="btn btn-primary btn-fill" href="{{action('RegisterController@returnVisitor', $g->id)}}">
                                                                Return
                                                            </button>

                                                            </td>
                                                        </form>
                                                    </tr>
                                                    <?php $jk++; ?>
                                                  @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                            </div>
                    </div>
                </div>

                <footer class="footer">
                    <div class="container-fluid">
                        <nav class="pull-left">
                            <ul>
                            <li>
                                <a href="{{ url('/') }}">
                                    Home
                                </a>
                            </li>
                        </ul>
                        </nav>
                        <p class="copyright pull-right">
                            &copy; <script>document.write(new Date().getFullYear())</script> <a href="">GMS</a>
                        </p>
                    </div>
                </footer>
            </div>

        </div>

        <!-- !. Table -->

        
    </body>
    
    <!--   Core JS Files   -->
    <script src="{{ URL::asset('../resources/assets/js/jquery-1.10.2.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('../resources/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="{{ URL::asset('../resources/assets/js/bootstrap-checkbox-radio-switch.js') }}"></script>

    <!--  Charts Plugin -->
    <script src="{{ URL::asset('../resources/assets/js/chartist.min.js') }}"></script>

    <!--  Notifications Plugin    -->
    <script src="{{ URL::asset('../resources/assets/js/bootstrap-notify.js') }}"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="{{ URL::asset('../resources/assets/js/light-bootstrap-dashboard.js') }}"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="{{ URL::asset('../resources/assets/js/demo.js') }}"></script>

    <script type="text/javascript">
        
        $(document).ready(function(){

            $('select.employee_id').change(function(){
                
                var str = $(this).val();
		var this_id = $(this).attr('id');

                // Get phone number
                $.ajax({
                    type : 'get',
		cache : false,
                    url : 'getnumber?number='+str,
                    success : function(data){
                    	$('#'+this_id).closest('tr').find('input.phone_no').val(data);
                    }
                });

                // Get employee names
                $.ajax({
                    type : 'get',
cache : false,
                    url : 'getfname?f_name='+str,
                    success : function(data){
                        $('#'+this_id).closest('tr').find('input.e_f_name').val(data);
                    }
                });
                $.ajax({
                    type : 'get',
cache: false,
                    url : 'getlname?l_name='+str,
                    success : function(data){
$('#'+this_id).closest('tr').find('input.e_l_name').val(data);
                        //$('#e_l_name').val(data);
                    }
                });

                // Get email
                $.ajax({
                    type : 'get',
cache : false,
                    url : 'getemail?email='+str,
                    success : function(data){
                        //$('#e_email').val(data);
$('#'+this_id).closest('tr').find('input.e_email').val(data);
                    }
                });
/*
                // Get email
                $.ajax({
                    type : 'get',
	cache : false,              
      url : 'getgcell?gcell='+str,
                    success : function(data){
                        //$('#g_cell').val(data);
$('#'+this_id).closest('tr').find('input.g_cell').val(data);
                    }
                });*/
            })
        })
    </script>

    <!-- Datatables -->
    <script type="text/javascript" charset="utf8" src="http://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#all_guests').DataTable();
        });
    </script>
</html>
