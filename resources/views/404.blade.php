<!DOCTYPE html>
<html>
    <head>
        <title>GMS Visitors</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <!-- Bootstrap core CSS     -->
        <link href="{{ URL::asset('../resources/assets/css/bootstrap.min.css') }}" rel="stylesheet" />

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }

            .btn {
                font-weight: 900;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <a class="btn btn-success" href="{{url('/')}}"> Home </a>
                <div class="title">Error Page 404. Sorry</div><br/>
            </div>
        </div>
    </body>
</html>
