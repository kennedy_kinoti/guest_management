<!DOCTYPE html>
<html>
  <head>
    <title>GMS Visitor {{ $visitor->id }}</title>
  </head>
  <body>
    <h1>Car {{ $visitor->id }}</h1>
    <ul>
      <li>Make: {{ $visitor->make }}</li>
      <li>Model: {{ $visitor->model }}</li>
      <li>Produced on: {{ $visitor->produced_on }}</li>
    </ul>
  </body>
</html>