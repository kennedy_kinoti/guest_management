<!DOCTYPE html>
<html>
    <head>
        <!-- <meta http-equiv="refresh" content="30" > -->
        <link type="image/png" href="{{ URL::asset('../resources/assets/img/fav.png') }}">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
  
        <title>GMS Visitors Registra</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

   

        <!-- Bootstrap core CSS     -->
        <link href="{{ URL::asset('../resources/assets/css/bootstrap.min.css') }}" rel="stylesheet" />

        <!-- Animation library for notifications   -->
        <link href="{{ URL::asset('../resources/assets/css/animate.min.css') }}" rel="stylesheet"/>

        <!--  Light Bootstrap Table core CSS    -->
        <link href="{{ URL::asset('../resources/assets/css/light-bootstrap-dashboard.css') }}" rel="stylesheet"/>


        <!--  CSS for Demo Purpose, don't include it in your project     -->
        <link href="{{ URL::asset('../resources/assets/css/demo.css') }}" rel="stylesheet" />


        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
        <link href="{{ URL::asset('../resources/assets/css/pe-icon-7-stroke.css') }}" rel="stylesheet" />

    </head>
    <body>
        <div class="wrapper">
            <div class="sidebar" data-color="purple" data-image="{{ URL::asset('../resources/assets/img/sidebar-5.jpg') }}">

                <!-- Side menu -->
                <div class="sidebar-wrapper">

                    <ul class="nav">
                        <li>
                            <a href="{{ url('/') }}">
                            <i class="pe-7s-note2"></i>
                                <p>Guests In</p>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/guests') }}">
                                <i class="pe-7s-news-paper"></i>
                                <p>Guests</p>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/visitor') }}">
                                <i class="pe-7s-user"></i>
                                <p>Add Visitor</p>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/staff') }}">
                                <i class="pe-7s-science"></i>
                                <p>Staff</p>
                            </a>
                        </li>
                        <li class="active">
                            <a href="{{ url('/card') }}">
                                <i class="pe-7s-graph"></i>
                                <p>Cards</p>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/reports') }}">
                                <i class="pe-7s-map-marker"></i>
                                <p>Reports</p>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- !. Side menu -->

            </div>


            <!-- Table -->
            <div class="main-panel">
                <nav class="navbar navbar-default navbar-fixed">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">Guests</a>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-left">
                                <li>
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-dashboard"></i>
                                        <p class="hidden-lg hidden-md">Dashboard</p>
                                    </a>
                                </li>
                            </ul>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <p>
                                            Options
                                            <b class="caret"></b>
                                        </p>

                                      </a>
                                      <ul class="dropdown-menu">
                                        <li><a href="{{ url('/register') }}">Register User</a></li>
                                        <li><a href="#">My Profile</a></li>
                                      </ul>
                                </li>
                                <li>
                                    <a href="{{ url('/logout') }}">
                                        <p>Log out</p>
                                    </a>
                                </li>
                                <li class="separator hidden-lg hidden-md"></li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="header">
                                        <h4 class="title">Add New Card</h4>
                                        <p class="category">Guest Management System</p>
                                    </div>
                                    <div class="content">
                                        <!-- Content -->
                                        {{ Form::open(array('url' => 'register_card')) }}

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Card Number:</label>
                                                        
                                                        <p>{{ Form::number('card_number', '', array('class'=>'form-control', 'required')) }}</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <p>{{ Form::submit('Submit',['class'=>'btn btn-info btn-fill']) }}</p>

                                        {{ Form::close() }}
                                        <!-- End of content -->
                                    </div>
                                </div>
                            </div>
                            </div>
                    </div>
                </div>

                <footer class="footer">
                    <div class="container-fluid">
                        <nav class="pull-left">
                            <ul>
                            <li>
                                <a href="{{ url('/public') }}/">
                                    Home
                                </a>
                            </li>
                        </ul>
                        </nav>
                        <p class="copyright pull-right">
                            &copy; <script>document.write(new Date().getFullYear())</script> <a href="">GMS</a>
                        </p>
                    </div>
                </footer>
            </div>

        </div>

        <!-- !. Table -->

        
    </body>
    <!--   Core JS Files   -->
    <script src="{{ URL::asset('../resources/assets/js/jquery-1.10.2.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('../resources/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="{{ URL::asset('../resources/assets/js/bootstrap-checkbox-radio-switch.js') }}"></script>

    <!--  Charts Plugin -->
    <script src="{{ URL::asset('../resources/assets/js/chartist.min.js') }}"></script>

    <!--  Notifications Plugin    -->
    <script src="{{ URL::asset('../resources/assets/js/bootstrap-notify.js') }}"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="{{ URL::asset('../resources/assets/js/light-bootstrap-dashboard.js') }}"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="{{ URL::asset('../resources/assets/js/demo.js') }}"></script>

</html>
