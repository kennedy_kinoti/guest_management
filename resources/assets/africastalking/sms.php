<?php
// Be sure to include the file you've just downloaded
require_once('AfricasTalkingGateway.php');

require("server.php");

// Specify your login credentials

// $username = "ken_kinoti";
// $apikey = "d26e7d7bb7619358bbce154d52537a2f3ace472573df67b91de5d7a716a5b206";

$username ="guest_management_system";
$apikey = "AfricasTalkingAPI";


// Specify the numbers that you want to send to in a comma-separated list
// Please ensure you include the country code (+254 for Kenya in this case)


$recipients = $_POST['phone_no'];

$guest_cell = $_POST['guest_cell'];

$visit = $_POST['employee_id'];

$visitor = $_POST['f_name'].' '.$_POST['l_name'];

// Specify your AfricasTalking shortCode or sender id
$from = "guest_management_system";

$visit = $visitor." has come to visit you and is waiting for you at the reception. 
Mobile Number ".$guest_cell;

// Create a new instance of our awesome gateway class
$gateway    = new AfricasTalkingGateway($username, $apikey);

// Any gateway error will be captured by our custom Exception class below, 
// so wrap the call in a try-catch block

try 
{ 
  // Thats it, hit send and we'll take care of the rest. 
  $results = $gateway->sendMessage($recipients, $visit, $from);
      
  foreach($results as $result) {
    print_r("Sending..... <br />");

    echo "<a href=\" /GMS/guest/public\">Click here to continue</a>";

    header('Refresh: 1;url = /GMS/guest/public');
  }
}
catch ( AfricasTalkingGatewayException $e )
{
  echo "Encountered an error while sending: ".$e->getMessage();
}

// DONE!!! 
