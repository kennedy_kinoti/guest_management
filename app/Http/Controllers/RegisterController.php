<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;

use App\Http\Requests;

use App\Register;

// here we use the mail namespace
use Mail;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    // List of all staff
    public function staff()
    {
        $staff = DB::select('SELECT * FROM employees ORDER BY f_name ASC');

        return view('staff', ['staff' => $staff]);
    }

    // Add new staff to the system
    public function add_staff()
    {
        return view('add_staff');
    }

    public function register_new_staff()
    {
        \App\Register::saveStaffData(Input::except(array('_token')));

        return \Redirect::to('/staff')->with('message', 'Somebody saaaave me');
    }

    // List of all cards
    public function card()
    {
        $cards = DB::select('SELECT visitor_card.card_number, visitor_card.status, status.condition FROM visitor_card INNER JOIN status ON visitor_card.status = status.id WHERE visitor_card.is_deleted = 0');

        return view('card', ['cards' => $cards]);
    }

    // Add new card to the system
    public function add_card()
    {
        return view('add_card');
    }

    // List of all visitors
    public function all_guests()
    {
        $guests = DB::select('SELECT visitors.id, visitors.f_name, visitors.l_name, visitors.guest_cell, visitors.national_id, visitors.employee_id, visitors.time_in, visitors.time_out, employees.f_name AS empfname, employees.l_name AS emplname FROM visitors INNER JOIN employees ON visitors.employee_id = employees.id WHERE status = 5 ORDER BY visitors.time_out DESC');

        $staff = DB::select('SELECT * FROM employees WHERE is_deleted = 0');

        $visitor_card = DB::select('SELECT * FROM visitor_card WHERE is_deleted = 0 AND status = 3');

        return view('all_guests', ['guests' => $guests, 'staff' => $staff, 'visitor_card' => $visitor_card]);

    }

    // List all present visitors
    public function guestsIn()
    {

        if ( \Auth::check() ) {
             $visitors = DB::select('SELECT visitors.id, visitors.f_name, visitors.l_name, visitors.guest_cell, visitors.national_id, visitors.employee_id, visitors.time_in, visitors.card_id, employees.f_name AS empfname, employees.l_name AS emplname FROM visitors INNER JOIN employees ON visitors.employee_id = employees.id WHERE status = 1 OR status = 0 ORDER BY visitors.time_in DESC');

        return view('welcome', ['visitors' => $visitors]);
        } else {

            
            return redirect()->action('Auth\AuthController@login');
        }

    }

    // Save to database
    public function store()
    {	
        \App\Register::saveFormData(Input::except(array('_token')));
    }

    // Send sms to the person who has visited
    public function registerVisitor()
    {   

        $destinationPath = app_path("../resources/assets/photos");
        $file = Input::file('passport_photo');
        
        // dd($destinationPath);

        if (Input::hasFile('passport_photo')) {
            $file->move($destinationPath, $file); 
        }

        // Save data to database
    	\App\Register::saveVisitorData(Input::except(array('_token')));

        $card_id = Input::get('card_id');

        // Update card status
        DB::table('visitor_card')
            ->where('id', $card_id)
            ->update([
                'status' => 4]);

        // Logic to send an sms to the person being visited
       include (app_path() .'../../resources/assets/africastalking/sms.php');

        // Logic to send an email to the person being visited
        $data = ['name' => Input::get('f_name').' '.Input::get('l_name')];

        // var_dump(Input::get('visitee_email'));
        Mail::send(['text'=>'mail'], $data, function($message)
        {
            $message->to(Input::get('visitee_email'))->subject('GMS Visitor');
            $message->from('kibugakimotho@gmail.com','GMS Security Desk');
        });

        // return \Redirect::to('/')->with('message', 'Hello');
        return \Redirect::to('/');

        // echo "Basics email was sent";
    }

    public function registerCard()
    {
        // Save data to database
        \App\Register::saveNewCard(Input::except(array('_token')));

        return \Redirect::to('/card')->with('message', 'Hello');
    }

// Shows all visitors as per how they have come
    public function show($id)
    {
    	// $visitor = Register::find($id);
     //    return view('show', array('visitor' => $visitor));	

        $returnGuest = DB::select('SELECT visitors.id, visitors.f_name, visitors.l_name, visitors.guest_cell, visitors.national_id, visitors.employee_id, visitors.time_in, visitors.passport_photo, employees.f_name AS empfname, employees.l_name AS emplname FROM visitors INNER JOIN employees ON visitors.employee_id = employees.id WHERE visitors.id = '.$id);

        return view('show', ['returnGuest' => $returnGuest]);

    }

// Remove a visitor. Checkout a guest
    public function exitVisitor()
    {
    	// dd('Button action is launched'.$id);
    	//Laravel update statement here 

        // dd(Input::all_guestsl());

    	DB::table('visitors')
    		->where('id', Input::get('id'))
    		->update([
                'status' => 5,
                'card_id' => 0,
                'time_out' => date('Y-m-d H:i:s')]);

        $card_id = Input::get('card_id');

        // dd($card_id);

        DB::table('visitor_card')
            ->where('id', $card_id)
            ->update([
                'status' => 3]);

        return \Redirect::to('/')->with('message', 'Somebody saaaave me');
    }

    // Reports Page
    public function reports(){
        
        $visitors = DB::select('SELECT visitors.id, visitors.f_name, visitors.l_name, visitors.national_id, visitors.employee_id, visitors.time_in, visitors.time_out, employees.f_name AS empfname, employees.l_name AS emplname FROM visitors INNER JOIN employees ON visitors.employee_id = employees.id ORDER BY visitors.time_out ASC');

        return view('reports', ['visitors' => $visitors]);
    }

    // Filter guests within date range
    public function filter()
    {   
        $from = Input::get('from');
        $to = Input::get('to');

        $visitors = DB::select('SELECT visitors.id, visitors.f_name, visitors.l_name, visitors.national_id, visitors.employee_id, visitors.time_in, visitors.time_out, employees.f_name AS empfname, employees.l_name AS emplname FROM visitors INNER JOIN employees ON visitors.employee_id = employees.id WHERE visitors.time_in >= "2017-5-10" AND visitors.time_out <= "2017-10-10" ORDER BY visitors.time_out ASC');

        return view('reports', ['visitors' => $visitors]);
    }

    // Returning Visitor
    public function returnVisitor()
    {   

        DB::table('visitors')
            ->where('id', Input::get('id'))
            ->update([
                'status' => 0,
                'employee_id' => Input::get('employee_id'),
                'card_id' => Input::get('card_id'),
                'time_in' => date('Y-m-d H:i:s'),
                'time_out' => 0,
                'visit_count' => 'visit_count' + 1]);

        Input::get('phone_no');
        Input::get('g_f_name');
        Input::get('g_l_name');
        Input::get('guest_cell');


        $card_id = Input::get('card_id');
        
        // Update card status
        DB::table('visitor_card')
            ->where('id', $card_id)
            ->update([
                'status' => 4]);

        // Logic to send an email to the person being visited
        $data = ['name' => Input::get('g_f_name').' '.Input::get('g_l_name')];

        return \Redirect::to('/');

    }

    // View a visitors details
    public function viewVisitor($id)
    {
        dd('Button action is launched'.$id);
    }

// Populate dropdown with all staff
    public function allStaff()
    {

        $staff = DB::select('SELECT * FROM employees WHERE is_deleted = 0 ORDER BY f_name ASC');

        $card = DB::select('SELECT * FROM visitor_card WHERE is_deleted = 0 AND status = 3');

        return view('visitor', ['staff' => $staff, 'card' => $card]);
    }

    public function getnumber(){

        $id = Input::get('number');

        $result = DB::table('employees')
            ->where('id', $id)->first();

        echo ($result) ? $result->phone_no : '';

    }

    public function getemail(){

        $id = Input::get('email');

        $result = DB::table('employees')
            ->where('id', $id)->first();

        echo ($result) ? $result->email : '';
    }

    public function getnationalid(){

        // $national_id = Input::get('national_id');

        // $result = DB::table('visitors')
        //     ->where('id', $national_id)->first();

        // echo ($result) ? $result->national_id : '';

        $id = $_GET['nationalid'];

       // $result = DB::select('SELECT * FROM visitors WHERE national_id = '.$id)->first();
        $result = DB::table('visitors')->where('national_id','=',$id)->first();
/*dd($result);
      

        if ($num_rows >  0)
    { echo "1"; }
        else{ echo "0"; }*/

        echo (is_null($result) || $result == "") ? "0" : '1';
        
    }

    public function getfname(){

        $id = Input::get('f_name');

        $result = DB::table('employees')
            ->where('id', $id)->first();

        echo ($result) ? $result->f_name : '';
    }

    public function getlname(){

        $id = Input::get('l_name');

        $result = DB::table('employees')
            ->where('id', $id)->first();

        echo ($result) ? $result->l_name : '';
    }

    public function getgcell(){

        $id = Input::get('gcell');

        $result = DB::table('visitors')
            ->where('id', $id)->first();

        echo ($result) ? $result->guest_cell : '';
    }

    public function logiout(){
        \Auth::logout();
        \Session::flush();
        return \Redirect::to('/');
    }
}
