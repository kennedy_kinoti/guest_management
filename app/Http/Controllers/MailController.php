<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

// here we use the mail namespace
use Mail;

class MailController extends Controller
{
    //Function for sending basic emails
    public function basic_email()
    {
    	$data = ['name' => 'John Matendechere'];
    	Mail::send(['text'=>'mail'], $data, function($message)
    	{
    		$message->from('securitydesk@strathmore.edu','Security Desk');
    	});
    	echo "Basics email was sent";
    }
}
