<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// login routes

Route::auth();


Route::get('/', 'RegisterController@guestsIn');

Route::get('visitor', 'RegisterController@allStaff')->middleware('auth');

// Make the views present 
// Route::get('register', function()
// {
// 	return View::make('register');
// })->middleware('auth');




// email routes
Route::get('/email','MailController@basic_email')->middleware('auth');

Route::post('register_action', 'RegisterController@store')->middleware('auth');

Route::post('register_visitor', 'RegisterController@registerVisitor')->middleware('auth');

Route::get('/mgeni/{id}', 'RegisterController@show')->middleware('auth');

// Url to exit a visitor from the system
Route::post('exit', 'RegisterController@exitVisitor')->middleware('auth');

// Return a guest back
//Route::get('/return/{id}', 'RegisterController@returnVisitor');
Route::post('return_visitor', 'RegisterController@returnVisitor')->middleware('auth');

// Url to view visitor details the system
Route::get('/view/{id}', 'RegisterController@viewVisitor')->middleware('auth');


// Get phone numbers of all employees
Route::get('getnumber','RegisterController@getnumber')->middleware('auth');

// Get email address of all employees
Route::get('getemail','RegisterController@getemail')->middleware('auth');

// Get national id of all registered guests
Route::get('getnationalid', 'RegisterController@getnationalid')->middleware('auth');

// Get names of employees
Route::get('getfname', 'RegisterController@getfname')->middleware('auth');
Route::get('getlname', 'RegisterController@getlname')->middleware('auth');

// Search for national ID
ROute::get('getnationalid', 'RegisterController@getnationalid')->middleware('auth');

// Guest filter
Route::get('reports', 'RegisterController@reports')->middleware('auth');
Route::post('filter', 'RegisterController@filter')->middleware('auth');

// All staff
Route::get('staff', 'RegisterController@staff')->middleware('auth');

// All guests
Route::get('guests', 'RegisterController@all_guests')->middleware('auth');

// Add staff URL
Route::get('addStaff', 'RegisterController@add_staff')->middleware('auth');

// register new staff
Route::post('register_staff', 'RegisterController@register_new_staff')->middleware('auth');

// Register new card
Route::get('card', 'RegisterController@card')->middleware('auth');

// Add card URL
Route::get('addCard', 'RegisterController@add_card')->middleware('auth');

Route::post('register_card', 'RegisterController@registerCard')->middleware('auth');

Route::get('/home', 'HomeController@index')->middleware('auth');

Route::get('logout', 'RegisterController@logiout')->middleware('auth');



Route::get('404', function(){
	return View::make('404');
});