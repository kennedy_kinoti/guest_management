<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    protected $guarded = array();
    protected $table = 'users';
    public $timestamps = 'false';

    // model function to store data from the database
    public static function saveFormData($data)
    {
    	\DB::table('users')->insert($data);
    }

    public static function saveVisitorData($data)
    {
        \DB::table('visitors')->insert($data);
    }

    public static function saveReturnVisitor($data)
    {
        \DB::table('visitors')->insert($data);
    }

    public static function getUsersData()
    {
    	\DB::table('users')->insert($users);

    	$value=DB::table('users')->orderBy('id', 'asc')->get();
    }

    public static function saveStaffData($data)
    {
        \DB::table('employees')->insert($data);
    }

    public static function saveNewCard($data)
    {
        \DB::table('visitor_card')->insert($data);
    }
}
