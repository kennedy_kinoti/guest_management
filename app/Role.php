<?php namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
	$owner = new Role();
	$owner->name         = 'owner';
	$owner->display_name = 'Project Owner'; // optional
	$owner->description  = 'User is the owner of a given project'; // optional
	$owner->save();

	$admin = new Role();
	$admin->name         = 'admin';
	$admin->display_name = 'User Administrator'; // optional
	$admin->description  = 'User is allowed to manage and edit other users'; // optional
	$admin->save();

	$user = User::where('username', '=', 'ken')->first();

	// role attach alias
	$user->attachRole($admin); // parameter can be an Role object, array, or id

	// or eloquent's original technique
	$user->roles()->attach($admin->id); // id only
}