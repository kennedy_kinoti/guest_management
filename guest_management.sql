-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 22, 2017 at 06:41 PM
-- Server version: 5.5.54-0ubuntu0.14.04.1
-- PHP Version: 5.6.30-7+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `guest_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(255) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_no` varchar(255) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `f_name`, `l_name`, `email`, `phone_no`, `is_deleted`) VALUES
(1, 'Kennedy', 'Mbabu', 'kkinoti@strathmore.edu', '0715916968', 0),
(2, 'B', 'C', 'bmuthaiga@strathmore.edu', '0725948611', 0),
(3, 'Wallace', 'Muchiri', 'wmuchiri@strathmore.edu', '0733299205', 0),
(4, 'Micheal', 'Gichure', 'mgichure@strathmore.edu', '0725165078', 0),
(5, 'Innocent', 'Qiprono', 'ikiprono@strathmore.edu', '0729394439', 0);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2017_01_23_133955_create_visitors_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `condition` varchar(255) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `condition`, `is_deleted`) VALUES
(1, 'In house', 0),
(2, 'Left', 0),
(3, 'Unassigned', 0),
(4, 'Assigned', 0),
(5, 'Lost', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Kennedy Kinoti', 'kkinoti@strathmore.edu', '$2y$10$vYV7F/Iz2laJyf216BLbQ.K.PoQz1tmTVrOWZ.Vr0HZb0SskTHzT6', '7vTxlVpz0VlQC64LbjwpVBOL32xwTU54dLbjysmfBWjuNu2xk2LRjUYINnLs', '2017-03-06 10:44:58', '2017-03-06 11:34:01'),
(2, 'Kennedy Kinoti', 'kinotikennedy@gmail.com', '$2y$10$t8g5QpfnQLXv3AlDNl5wsOb1j2AyCswWagASLrrEIh3LKZLyKCYxC', 'NmmRiZ9JHrAYCRC8ZT3Qm5y8LHCTnxahwUBqMWUkCTiahtfEw7FYud61RYkO', '2017-03-06 11:35:25', '2017-03-21 09:30:09');

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE IF NOT EXISTS `visitors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `f_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `l_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `guest_cell` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `national_id` int(11) NOT NULL,
  `employee_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visitee_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `card_id` int(11) NOT NULL,
  `time_in` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time_out` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `passport_photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `national_id` (`national_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `visitors`
--

INSERT INTO `visitors` (`id`, `f_name`, `l_name`, `guest_cell`, `national_id`, `employee_id`, `phone_no`, `visitee_email`, `card_id`, `time_in`, `time_out`, `passport_photo`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Morgan ', 'Heritage', '0712345678', 30165606, '1', '0715916968', 'kkinoti@strathmore.edu', 3, '2017-03-22 17:57:11', '0', '/tmp/php3KtYMH', '0', NULL, NULL),
(2, 'Richie', 'Spice', '0713243234', 30165607, '1', '0715916968', 'kkinoti@strathmore.edu', 1, '2017-03-22 17:58:04', '0', '/tmp/phpVHE7CG', '0', NULL, NULL),
(3, 'Jah ', 'Cure', '0790876543', 30165608, '1', '0715916968', 'kkinoti@strathmore.edu', 1, '2017-03-20 10:20:05', '2017-03-22 14:31:36', '/tmp/phpMLjOYr', '0', NULL, NULL),
(4, 'James', 'Bilpo', '0712345678', 30165609, '1', '', '', 1, '21-03-2017 12:44:20', '2017-03-22 14:31:40', '/tmp/phppB8lZT', '0', NULL, NULL),
(5, 'f_name', 'l_name', '0712432343', 12, '1', '', '', 1, '2017-03-22 18:37:36', '2017-03-22 18:40:03', '/tmp/phpqUBfkU', '5', NULL, NULL),
(6, 'Bi', 'Scale', '0712097809', 123, '1', '', '', 1, '2017-03-22 18:40:12', '0', '/tmp/php1mrtWd', '0', NULL, NULL),
(7, 'Joe', 'Morgan', '0987124352', 1234, '1', '', '', 2, '2017-03-22 17:54:22', '2017-03-22 18:16:55', '/tmp/phpD4UfXo', '0', NULL, NULL),
(8, 'Mit', 'Morgan', '0787124352', 12345, '1', '', '', 3, '2017-03-22 17:54:41', '2017-03-22 18:16:57', '/tmp/phpf58ZBn', '0', NULL, NULL),
(9, 'Jack', 'Morgan', '0797124352', 123456, '1', '', '', 2, '2017-03-22 17:56:52', '2017-03-22 17:58:21', '/tmp/php6jrSLf', '0', NULL, NULL),
(10, 'Jackton', 'Morgan', '0798124352', 1234567, '1', '', '', 3, '21-03-2017 13:00:38', '2017-03-22 17:58:21', '/tmp/phpgFW3bi', '0', NULL, NULL),
(11, 'Wendy', 'Morgan', '0790124352', 12345678, '1', '', '', 4, '21-03-2017 13:00:38', '', '/tmp/phppQC04u', '', NULL, NULL),
(13, 'Ted', 'Luke', '0712412321', 2, '1', '', '', 1, '2017-03-22 18:36:51', '2017-03-22 18:39:17', '/tmp/php1StzyL', '5', NULL, NULL),
(14, 'Kennedy', 'Kinoti', '0715916968', 23, '1', '', '', 1, '2017-03-22 18:39:26', '2017-03-22 18:40:04', '/tmp/phpmWcMmg', '5', NULL, NULL),
(15, 'Tonny', 'Montana', '0715916968', 10000000, '1', '', 'kkinoti@strathmore.edu', 7, '22-03-2017 13:29:56', '2017-03-22 17:58:22', '/tmp/phpsSgVNI', '0', NULL, NULL),
(16, 'Jeff', 'Benzos', '0715916968', 10000001, '1', '0715916968', 'kkinoti@strathmore.edu', 7, '22-03-2017 13:29:56', '2017-03-22 17:58:23', '/tmp/phpemM1L5', '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `visitor_card`
--

CREATE TABLE IF NOT EXISTS `visitor_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card_number` int(11) NOT NULL,
  `status` varchar(45) DEFAULT '3',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `visitor_card`
--

INSERT INTO `visitor_card` (`id`, `card_number`, `status`, `is_deleted`) VALUES
(1, 1, '4', 0),
(2, 2, '4', 0),
(3, 3, '4', 0),
(4, 4, '4', 0),
(5, 5, '3', 0),
(6, 6, '4', 0),
(7, 7, '4', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
